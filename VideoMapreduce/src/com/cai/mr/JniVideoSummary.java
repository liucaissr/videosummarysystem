package com.cai.mr;

public class JniVideoSummary {
	static {
		// 调用文件名为JNI Library.dll的动态库
		System.load("/Users/my/Documents/JavaOpenCV/JNI Test/resources/libVideoSummaryLib.dylib");
	}
	// native方法声明
	public native String videoSummary(String localPath);

	public native long getFrameSize(String localPath);

}
